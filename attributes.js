var pkcs11js = require("pkcs11js");
var configuration = require("./config.json");

console.log("Config Lib= "+configuration.lib+", PIN="+configuration.pin+" , Slot="+configuration.slot);

var AESKeyName = {}; 
if ((process.argv[2] !== null) && (typeof process.argv[2] != "undefined")) {
	AESKeyName = process.argv[2];
}
else {
	console.log("No Keyname provided - exit");
	//process.exit(-1);
	return;
}


console.time("PKCS11 Initial");
console.time("Program Time");
var pkcs11 = new pkcs11js.PKCS11();
pkcs11.load(configuration.lib);
pkcs11.C_Initialize();
var session;


try {
    // Getting info about PKCS11 Module
//    var module_info = pkcs11.C_GetInfo();

    // Getting list of slots
    var slots = pkcs11.C_GetSlotList(true);
    var slot = slots[configuration.slot];
    
    console.time("PKCS11 Session Time");

    session = pkcs11.C_OpenSession(slot, pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION);
    pkcs11.C_Login(session, 1, configuration.pin);
    console.timeEnd("PKCS11 Initial");
    console.timeEnd("PKCS11 Session Time");
    

    var AES_SearchTempalte = [
    	{type: pkcs11js.CKA_LABEL, value: AESKeyName},
       // {type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
    ];
    try {
       pkcs11.C_FindObjectsFinal(session); // just in case
    } catch (e) {};
    
    console.time("C_FindObjects Time");
    var rc = pkcs11.C_FindObjectsInit(session,AES_SearchTempalte);
    var hObject = pkcs11.C_FindObjects(session);
    console.timeEnd("C_FindObjects Time");
    //console.log("Found Object =", hObject);
    if (hObject !== null ) {
    	// First Get Attributes
        console.time("C_GetAttributeValue Time");
        var inAttributes  = pkcs11.C_GetAttributeValue(session, hObject, [
            { type: pkcs11js.CKA_LABEL },
            { type: pkcs11js.CKA_CLASS },
            { type: pkcs11js.CKA_END_DATE },
            { type: pkcs11js.CKA_KEY_TYPE},
        ]);    	
        console.timeEnd("C_GetAttributeValue Time");
        console.log("---------- Key Found ---------------\n");
        console.log("Named Attributes :\n", inAttributes );
        
        console.log(" CKA_LABEL = ", inAttributes [ 0 ].value.toString());
        console.log(" CKA_CLASS = ", inAttributes [ 1 ].value.readUInt32LE());
        console.log(" CKA_END_DATE = ",inAttributes [ 2 ].value.toString());
        console.log(" CKA_KEY_TYPE = ",inAttributes [ 3 ].value.toString());
        console.log("------------------------------------\n");
        
//        rc = pkcs11.C_SetAttributeValue(session, hObject, [
//			{ type: pkcs11js.CKA_LABEL, value: "NewKey1"},
//			{ type: pkcs11js.CKA_END_DATE, value: "20171130"},
//			{ type: pkcs11js.CKA_ID, value: "NODEJS_ID1"},
//			]);
        
        //pkcs11.C_DestroyObject(session, hObject);
    }
    else {
        console.log("--------- Key Not Found ----------\n");
    }
}
catch(e){
    console.error(e);
}
finally {
    console.time("PKCS11 Cleanup Time");
    if (session !== undefined && Object.getOwnPropertyNames(session).length !== 0) {
        // Only try logout and close session when session is defined and non zero
        pkcs11.C_Logout(session);
        pkcs11.C_CloseSession(session);
    }
    pkcs11.C_Finalize();
    console.timeEnd("PKCS11 Cleanup Time");
}

console.timeEnd("Program Time");
