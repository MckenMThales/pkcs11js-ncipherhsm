/**
 * http://usejsdoc.org/
 */
var pkcs11js = require("pkcs11js");
var configuration = require("./config.json");

console.log("Config Lib= "+configuration.lib+", PIN="+configuration.pin+" , Slot="+configuration.slot);

var AESKeyName = {}; 
if ((process.argv[2] !== null) && (typeof process.argv[2] != "undefined")) {
	AESKeyName = process.argv[2];
}
else {
	console.log("No Keyname provided - exit");
	//process.exit(-1);
	return;
}



var pkcs11 = new pkcs11js.PKCS11();
pkcs11.load(configuration.lib);
// var mod = Module.load(configuration.lib, "Vormetric PKCS#11 Agent");
pkcs11.C_Initialize();
var session;
try {
    // Getting info about PKCS11 Module
    var module_info = pkcs11.C_GetInfo();

    // Getting list of slots
    var slots = pkcs11.C_GetSlotList(true);
    var slot = slots[configuration.slot];

    session = pkcs11.C_OpenSession(slot, pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION);

    // Getting info about Session
    var info = pkcs11.C_GetSessionInfo(session);
    pkcs11.C_Login(session, 1 , configuration.pin);
    

    var AES_SearchTempalte = [
    	{type: pkcs11js.CKA_LABEL, value: AESKeyName},
        {type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
    ];
    try {
        pkcs11.C_FindObjectsFinal(session); // just in case
    } catch (e) {};  // Just ignore
    
    pkcs11.C_FindObjectsInit(session,AES_SearchTempalte);
    var hObject = pkcs11.C_FindObjects(session);
    if (hObject !== null ) {
    	// First Get Attributes
        console.log("Key",AESKeyName," Found, deleting - Obj.len="+hObject.length);
        pkcs11.C_DestroyObject(session, hObject);
    }
    else {
        console.log("Key",AESKeyName,"Not found");
    }
    console.log("end of seach --- ");
}
catch(e){
	console.log("Exception:::::");
    console.error(e);
}
finally {
    if (session !== undefined && Object.getOwnPropertyNames(session).length !== 0) {
        // Only try logout and close session when session is defined and non zero
        pkcs11.C_Logout(session);
        pkcs11.C_CloseSession(session);
    }
    pkcs11.C_Finalize();
}