# pkcs11js test project codes
This project contains

* config.json HSM access parameters for the pkcs11 API

* package.json  NPM package that describe the project requirements and dependency

* pkcstest.js : general test app that list slots, mechanisms, create key (remove existing), change attribute and remove key 

* attribute.js : list attributes of a key

* generateKey.js - generate a AES-256 key

* careateObjKey.js - create a Object key with fix value - Test only - will fail in StrictFIPS mode

* findAdnDelete.js - Find a given key and delete it form local


## Install
Install all required packages before using any of the applications 
run : npm install


## Usage
Configuraiton uses config.json to describe the DSM parameters to the applications 
```
{
    "lib": ""C:\\Program Files (x86)\\nCipher\\nfast\\toolkits\\pkcs11\\cknfast-64.dll",
    "libName": "nCipher Corp. Ltd",
	"slot":0,
    "pin": ""
}
```

PS: May want to comment out all the C_Login if PIN is need, or set CKNFAST_FAKE_ACCELERATOR_LOGIN accordinly


# How to run test
* pkcstest.js 
    * run : node pkcetest.js [Key Name]

* attribute.js 
	* run : node attribute.js [Key Name]

* generateKey.js 
	* run : node generateKey.js [Key Name]

* careateObjKey.js 
	* run : node careateObjKey.js [Key Name]

* exportKey.js
	* run : node exportKey.js [Key Name]
	
* findAdnDelete.js
	* run : node findAdnDelete.js [Key Name]





##


## Developing



### Tools