/**
 * http://usejsdoc.org/
 */
var pkcs11js = require("pkcs11js");
var configuration = require("./config.json");
var session;

console.log("Config Lib="+configuration.lib+", PIN="+configuration.pin+" , Slot="+configuration.slot);

var pkcs11 = new pkcs11js.PKCS11();
//pkcs11.load(configuration.lib);
pkcs11.load(configuration.lib, configuration.libName);

pkcs11.C_Initialize();
try {
    // Getting info about PKCS11 Module
    var module_info = pkcs11.C_GetInfo();

    // Getting list of slots
    var slots = pkcs11.C_GetSlotList(true);
    var slot = slots[configuration.slot];

    // Getting info about slot
    var slot_info = pkcs11.C_GetSlotInfo(slot);
    // Getting info about token
    var token_info = pkcs11.C_GetTokenInfo(slot);

    // Getting info about Mechanism
    var mechs = pkcs11.C_GetMechanismList(slot);
    var mech_info = pkcs11.C_GetMechanismInfo(slot, mechs[0]);
    session = pkcs11.C_OpenSession(slot, pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION);


    // List Mechanisms
    console.log("Slot #" + configuration.slot);
    console.log("\tmanufacturerID:", slot_info.manufacturerID);
    console.log("\tDescription:", slot_info.slotDescription);
    console.log("\tSerial:", token_info.serialNumber);
    console.log("\tPassword(min/max): %d/%d", token_info.minPinLen, token_info.maxPinLen);
    console.log("\tIs hardware:", !!(slot_info.flags & pkcs11js.CKF_HW_SLOT));
    console.log("\tIs removable:", !!(slot_info.flags & pkcs11js.CKF_REMOVABLE_DEVICE));
    console.log("\tIs initialized:", !!(slot_info.flags & pkcs11js.CKF_TOKEN_PRESENT));
    console.log("\n\nMechanisms:");
    console.log("CKM ID                     h/s/v/e/d/w/u");
    console.log("========================================");
    
    function b(v) {
        return v ? "+" : "-";
    }

    function s(v) {
        v = v.toString();
        for (var i_1 = v.length; i_1 < 27; i_1++) {
            v += " ";
        }
        return v;
    }

    for (var j = 0; j < mechs.length; j++) {
    	var mech = pkcs11.C_GetMechanismInfo(slot, mechs[j]);
        console.log(s(mechs[j].toString(16)) +
            b(mech.flags & pkcs11js.CKF_DIGEST) + "/" +
            b(mech.flags & pkcs11js.CKF_SIGN) + "/" +
            b(mech.flags & pkcs11js.CKF_VERIFY) + "/" +
            b(mech.flags & pkcs11js.CKF_ENCRYPT) + "/" +
            b(mech.flags & pkcs11js.CKF_DECRYPT) + "/" +
            b(mech.flags & pkcs11js.CKF_WRAP) + "/" +
            b(mech.flags & pkcs11js.CKF_UNWRAP));
    }
    
    
    // Getting info about Session
    var info = pkcs11.C_GetSessionInfo(session);
    //pkcs11.C_Login(session, 1, configuration.pin);
    
    /**
    * Your app code here
    */
    var AESKeyName = "ThalesTestKey"; 
    if ((process.argv[2] !== null) && (typeof process.argv[2] != "undefined")) {
    	AESKeyName = process.argv[2];
    }    
    console.log("Test Key Name = "+AESKeyName);
    var AESExpKeyName = "TestKeyUnknown";
//    var MyKeyValue = "0123456789abcdef0123456789abc";

    var AES_SearchTempalte = [
    	{type: pkcs11js.CKA_LABEL, value: AESKeyName},
        {type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
    ];
    //pkcs11.C_FindObjectsFinal(session); // just in case - not work for HSM
    var randData = new Buffer(16);
    var rc = pkcs11.C_GenerateRandom(session,randData);
    console.log("C_GenerateRandom Data=",randData);
    
    //pkcs11.C_FindObjectsInit(session);
     rc = pkcs11.C_FindObjectsInit(session,AES_SearchTempalte);
    var hObject = pkcs11.C_FindObjects(session);
    if (hObject !== null ) {  // If key exist - list its detail then remove the key before create one 
        console.log("Key "+AESKeyName+" found, removing\nObject =",hObject);
    	// First Get Attributes
        var label = pkcs11.C_GetAttributeValue(session, hObject, [
            { type: pkcs11js.CKA_LABEL },
            { type: pkcs11js.CKA_CLASS },
            { type: pkcs11js.CKA_TOKEN },
            { type: pkcs11js.CKA_ID },
            //{ type: pkcs11js.CKA_VALUE },
            { type: pkcs11js.CKA_END_DATE },
        ]);    	
        console.log(AESKeyName," Attributes = ", label, "Date=",JSON.stringify(label[ 3 ].value));
        // Export the key

        var invBuff = new Buffer([0,0,0,0,0,0,0,0]);
        var RecBuff = new Buffer(32);
        console.log("inv=",invBuff);
        
        var iv = new Buffer([0x80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        var ExportKey = pkcs11.C_WrapKey(session,
        		{ mechanism: pkcs11js.CKM_AES_CBC_PAD, parameter: iv  },
        		invBuff,
        		hObject, 
        		RecBuff);
        console.log ("Exporting key = ", ExportKey);
        
        
        pkcs11.C_DestroyObject(session, hObject);
    }
    
    
    // Create a key with the given KeyName
    var AES_template = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
        { type: pkcs11js.CKA_TOKEN, value: true },
        { type: pkcs11js.CKA_KEY_TYPE, value: pkcs11js.CKK_AES}, 
        { type: pkcs11js.CKA_LABEL, value: AESKeyName},
        { type: pkcs11js.CKA_VALUE_LEN, value: 256 / 8 },
        { type: pkcs11js.CKA_PRIVATE, value: true },
        { type: pkcs11js.CKA_ENCRYPT, value: true },
        { type: pkcs11js.CKA_DECRYPT, value: true },
        { type: pkcs11js.CKA_VERIFY, value: true },
        { type: pkcs11js.CKA_SENSITIVE, value: true },
        { type: pkcs11js.CKA_EXTRACTABLE, value: false },
//        { type: pkcs11js.CKA_VALUE, value: MyKeyValue },        
    ];
    
    var key = pkcs11.C_GenerateKey(session,{ mechanism: pkcs11js.CKM_AES_KEY_GEN },AES_template);
    console.log ("C_GenerateKey = ", key);
    // Test set Key attribute
    rc = pkcs11.C_SetAttributeValue(session, key, [{ type: pkcs11js.CKA_END_DATE, value: "20161110"}]);
    
    pkcs11.C_DestroyObject(session, key);
    console.log ("C_DestroyObject Key");

}
catch(e){
    console.error(e);
}
finally {
    if (session !== undefined && Object.getOwnPropertyNames(session).length !== 0) {
        // Only try logout and close session when session is defined and non zero
        pkcs11.C_Logout(session);
        pkcs11.C_CloseSession(session);
    }    
    pkcs11.C_Finalize();
}