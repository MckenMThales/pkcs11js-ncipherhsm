/**
 * http://usejsdoc.org/
 */
var pkcs11js = require("pkcs11js");
var configuration = require("./config.json");

console.log("Config Lib= "+configuration.lib+", PIN="+configuration.pin+" , Slot="+configuration.slot);
var KeyName = {}; 
if ((process.argv[2] !== null) && (typeof process.argv[2] != "undefined")) {
	KeyName = process.argv[2];
}
else {
	console.log("Need AES Key name as parameter, usage - node createObjKey.js [KeyName]\n");
	return ;
}
console.log("KeyName uses = ",KeyName);

var pkcs11 = new pkcs11js.PKCS11();
pkcs11.load(configuration.lib);
pkcs11.C_Initialize();
var session;  // PKCS11 Session

try {
    // Getting info about PKCS11 Module
    var module_info = pkcs11.C_GetInfo();

    // Getting list of slots
    var slots = pkcs11.C_GetSlotList(true);
    var slot = slots[configuration.slot];

    // Getting info about slot
    var slot_info = pkcs11.C_GetSlotInfo(slot);
    // Getting info about token
    var token_info = pkcs11.C_GetTokenInfo(slot);

    // Getting info about Mechanism
    var mechs = pkcs11.C_GetMechanismList(slot);
    var mech_info = pkcs11.C_GetMechanismInfo(slot, mechs[0]);
    session = pkcs11.C_OpenSession(slot, pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION);
    pkcs11.C_Login(session, 1, configuration.pin);

    // Getting info about Session
    var info = pkcs11.C_GetSessionInfo(session);
//    pkcs11.C_Login(session, 1, configuration.pin);
    

    var AES_SearchTempalte = [
    	{type: pkcs11js.CKA_LABEL, value: KeyName},
        {type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
    ];
    //pkcs11.C_FindObjectsFinal(session); // just in case
    
    var rc = pkcs11.C_FindObjectsInit(session,AES_SearchTempalte);
    var hObject = pkcs11.C_FindObjects(session);
    console.log("Found Object =", hObject);
    if (hObject !== null ) {
    	// First Get Attributes
        var label = pkcs11.C_GetAttributeValue(session, hObject, [
            { type: pkcs11js.CKA_LABEL },
            { type: pkcs11js.CKA_CLASS },
        ]);    	
        console.log("Key",KeyName," Found, deleting");
        pkcs11.C_DestroyObject(session, hObject);
    }
    
    
    var AES_template = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
        { type: pkcs11js.CKA_TOKEN, value: true },
        { type: pkcs11js.CKA_KEY_TYPE, value: pkcs11js.CKK_AES}, 
        { type: pkcs11js.CKA_LABEL, value: KeyName},
        { type: pkcs11js.CKA_PRIVATE, value: true },
        { type: pkcs11js.CKA_VALUE_LEN, value: 256 / 8 },
        { type: pkcs11js.CKA_ENCRYPT, value: true },
        { type: pkcs11js.CKA_DECRYPT, value: true },
        { type: pkcs11js.CKA_SIGN, value: true },
        { type: pkcs11js.CKA_VERIFY, value: true },
        { type: pkcs11js.CKA_WRAP, value: true },
        { type: pkcs11js.CKA_UNWRAP, value: true },
        { type: pkcs11js.CKA_SENSITIVE, value: true },
        { type: pkcs11js.CKA_EXTRACTABLE, value: false },
    ];
//    var key = pkcs11.C_CreateObject(session,AES_template);
    var key = pkcs11.C_GenerateKey(session,{ mechanism: pkcs11js.CKM_AES_KEY_GEN },AES_template);
    console.log ("C_GenerateKey = ", key);
}
catch(e){
    console.error(e);
}
finally {
    if (session !== undefined && Object.getOwnPropertyNames(session).length !== 0) {
        // Only try logout and close session when session is defined and non zero
        pkcs11.C_Logout(session);
        pkcs11.C_CloseSession(session);
    }
    pkcs11.C_Finalize();
}