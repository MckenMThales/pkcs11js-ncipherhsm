/**
 * http://usejsdoc.org/
 */
var pkcs11js = require("pkcs11js");
var configuration = require("./config.json");
var session;

console.log("Config Lib= "+configuration.lib+", PIN="+configuration.pin+" , Slot="+configuration.slot);
var KeyName = {}; 
if ((process.argv[2] !== null) && (typeof process.argv[2] != "undefined")) {
	KeyName = process.argv[2];
}
else {
	console.log("Need AES Key name as parameter, usage - node createObjKey.js [KeyName]\n");
	return ;
}
console.log("KeyName uses = ",KeyName);

var pkcs11 = new pkcs11js.PKCS11();
pkcs11.load(configuration.lib);
// var mod = Module.load(configuration.lib, "Vormetric PKCS#11 Agent");
pkcs11.C_Initialize();

try {
    // Getting info about PKCS11 Module
    var module_info = pkcs11.C_GetInfo();

    // Getting list of slots
    var slots = pkcs11.C_GetSlotList(true);
    var slot = slots[configuration.slot];

    session = pkcs11.C_OpenSession(slot, pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION);

    // Getting info about Session
    var info = pkcs11.C_GetSessionInfo(session);
    console.log("Session Info = ", info);
    pkcs11.C_Login(session, 1, configuration.pin);
    
    var MyKeyValue = "0123456789abcdef0123456789abcef";

    var SearchTempalte = [
    	{type: pkcs11js.CKA_LABEL, value: KeyName},
    	// Any Class of key        {type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
    	{type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
    ];
    pkcs11.C_FindObjectsFinal(session); // just in case
    // Get an invalid handle
    
    var rc = pkcs11.C_FindObjectsInit(session,SearchTempalte);
    var hObject = pkcs11.C_FindObjects(session);
    console.log("Found Object =", hObject);
    if (hObject !== null ) {
    	// First Get Attributes
        var label = pkcs11.C_GetAttributeValue(session, hObject, [
            { type: pkcs11js.CKA_LABEL },
            { type: pkcs11js.CKA_CLASS },
            { type: pkcs11js.CKA_TOKEN },
            { type: pkcs11js.CKA_END_DATE },
            { type: pkcs11js.CKA_ID },
            { type: pkcs11js.CKA_VALUE },
            { type: pkcs11js.CKA_VALUE_LEN },
            { type: pkcs11js.CKA_VALUE_BITS },
        ]);    	
        //console.log(KeyName," Attributes:\n", label, "\nDate=",label[ 3 ].value.toString());
        console.log("VLen-",label[6].value.readUInt32LE(0)," , VBitLen=",label[7].value.readUInt32LE(0));
        // Export the key
//        var invBuff = new Buffer(0);
        var invBuff = new Buffer([0,0,0,0,0,0,0,0]);
        var RecBuff = new Buffer(32);
        console.log("inv=",invBuff);"         		"
        
        var iv = new Buffer([0x80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
        var ExportKey = pkcs11.C_WrapKey(session,
        		{ mechanism: pkcs11js.CKM_AES_CBC_PAD, parameter: iv  },
        		invBuff,
        		hObject, 
        		RecBuff);
        console.log ("Exporting key = ", ExportKey, " Len=", ExportKey.length);
        
        
        //pkcs11.C_DestroyObject(session, hObject);
    }
    else {
    	console.log("Key not found to Export!!");
    }
    
    console.log("end of Export --- ");
    
    
/*    var AES_template = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
        { type: pkcs11js.CKA_TOKEN, value: true },
        { type: pkcs11js.CKA_KEY_TYPE, value: pkcs11js.CKK_AES}, 
        { type: pkcs11js.CKA_LABEL, value: KeyName},
        { type: pkcs11js.CKA_VALUE_LEN, value: 256 / 8 },
        { type: pkcs11js.CKA_ENCRYPT, value: true },
        { type: pkcs11js.CKA_DECRYPT, value: true },
        { type: pkcs11js.CKA_VERIFY, value: true },
        { type: pkcs11js.CKA_APPLICATION, value: "NodeJS TestApp" },
        { type: pkcs11js.CKA_VALUE, value: MyKeyValue },        
    ];
*/    
//    var key = pkcs11.C_GenerateKey(session,{ mechanism: pkcs11js.CKM_AES_KEY_GEN },AES_template);
    
    
//    rc = pkcs11.C_SetAttributeValue(session, key, [{ type: pkcs11js.CKA_END_DATE, value: {year:"2016", month:"12", day:"30"} }]);
//    rc = pkcs11.C_SetAttributeValue(session, key, [{ type: pkcs11js.CKA_END_DATE, value: "20161110"}]);
    
    //rc = pkcs11.C_SetAttributeValue(session, key, [{ type: pkcs11js.CKA_VALUE, value: "Test Value" }]);
//    console.log("set Att rc =",rc); 
    
//    console.log("Just created Key=",key);
    

}
catch(e){
    console.error(e);
}
finally {
    if (session !== undefined && Object.getOwnPropertyNames(session).length !== 0) {
        // Only try logout and close session when session is defined and non zero
        pkcs11.C_Logout(session);
        pkcs11.C_CloseSession(session);
    }
    pkcs11.C_Finalize();
}